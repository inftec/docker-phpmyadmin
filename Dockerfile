FROM ubuntu:14.04
MAINTAINER Fabian Gut <fabian.gut@inftec.ch>, InfTec GmbH

ENV PHPMYADMIN_VERSION 4.3.7

RUN apt-get update && \
    apt-get install -y \
        apache2 \
        curl \
        fonts-dejavu-core \
        libapache2-mod-php5 \
        mysql-client \
        perl \
        php-gettext \
        php5-gd \
        php5-json \
        php5-mcrypt \
        php5-mysql

RUN curl -Lks http://downloads.sourceforge.net/project/phpmyadmin/phpMyAdmin/${PHPMYADMIN_VERSION}/phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages.tar.gz -o /root/phpmyadmin.tar.gz
RUN rm /var/www/html/index.html && tar xzf /root/phpmyadmin.tar.gz --strip=1 -C /var/www/html
ADD config.inc.php /var/www/html/config.inc.php
RUN chmod 644 /var/www/html/config.inc.php

EXPOSE 80
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

